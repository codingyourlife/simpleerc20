# SimpleErc20 Token

This project is a simple [ERC20 token](https://theethereum.wiki/w/index.php/ERC20_Token_Standard) created with zeplin framework. The [openzeppelin](https://openzeppelin.org/) StandardToken has been extended with two demo function Me() to display the `msg.sender` address and the function MyBalance() to display the `balances[msg.sender]` balance.

A [web3.js](https://github.com/ethereum/web3.js/) demo page is available but needs some configuration work.

## Contract Deployments

### Ropsten testnet
|Contract|Address|
| --|--|
|SimpleErc20Token|[0x39b1cc5c0fcd0d47929fab39e8fc58f90dcc2704](https://ropsten.etherscan.io/address/0x39b1cc5c0fcd0d47929fab39e8fc58f90dcc2704)|

## Using the Contract - Contract Demo

Clone this repository and run the *index.html* file in your browser

###Requirements
The demo is based on Gary Simon's [Interacting with a Smart Contract through Web3.js (Tutorial)](https://coursetro.com/posts/code/99/Interacting-with-a-Smart-Contract-through-Web3.js-(Tutorial))

- run `testrpc`
- run `remixd -S <absolute-path-to-project-shared-folder> ` (replace placeholder with local path) [full instructions](https://ethereum.stackexchange.com/a/38954/31781)
- set environment to Web3Provider

![setEnvironmentToWeb3Provider](images/setEnvironmentToWeb3Provider.png)

- select and then Run > Create the *SimpleErc20Token.sol* contract

![selectContract](images/selectContract.png)

![simpleErc20Creation](images/simpleErc20Creation.png)

- locate, copy, insert ABI into form like this

![tokenDetailsButton](images/tokenDetailsButton.png)

![tokenDetailsAbi](images/tokenDetailsAbi.png)

- locate, copy, insert Token-Value from the created token

![copyTokenAddress](images/copyTokenAddress.png)

You can now control the form in your browser and get results from the contract.

## Screenshots of Contract Demo
On a fresh contract account number 0 should have all the *INITIAL_SUPPLY* as balance and account number 1-9 should have 0 balance.

![accountNr0](images/accountNr0.png)
![accountNr1](images/accountNr1.png)

## Deploy to Test Net
The deployment is mainly based on Mason Forest's tutorial [How to deploy an ERC20 token in 20 minutes](http://www.masonforest.com/blockchain/ethereum/2017/11/13/how-to-deploy-an-erc20-token-in-20-minutes.html)
- run `npm install --save-dev dotenv truffle-wallet-provider ethereumjs-wallet`
- create a file called *.env* in the project root
- add the follwing to the file (private key and ETH from faucet can be retrieved via metamask):
ROPSTEN_PRIVATE_KEY="123YourPrivateKeyHere"
- run `truffle deploy --network ropsten`
