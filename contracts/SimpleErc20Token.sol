pragma solidity ^0.4.18;

import '../node_modules/zeppelin-solidity/contracts/token/ERC20/StandardToken.sol';

/*
* Simple ERC20 Token for demonstrations purposes
* Note: Filename must match contract name or deployment will fail
*/
contract SimpleErc20Token is StandardToken {
  string public name = "DEMO COIN";
  string public symbol = "JMT";
  uint public decimals = 18;

  uint public INITIAL_SUPPLY = 45000000;

  function SimpleErc20Token() public {
    balances[msg.sender] = INITIAL_SUPPLY;
  }
  
  function Me() public view returns(address myAddress){
      return msg.sender;
  }
  
  function MyBalance() public view returns(uint myBalance){
      return balances[msg.sender];
  }
}
